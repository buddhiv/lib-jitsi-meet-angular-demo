/**
 * Created by Pranav on 12/11/2015.
 */
app.service('PresenterEffect', function () {
    class PresenterEffect {
        constructor(videoStream) {

            console.log('>>> in presenter effect 111');

            this._canvas;
            this._ctx;
            this._desktopElement;
            this._desktopStream;
            this._frameRate;
            this._onVideoFrameTimer;
            this._onVideoFrameTimerWorker;
            this._renderVideo;
            this._videoFrameTimerWorker;
            this._videoElement;
            this.isEnabled;
            this.startEffect;
            this.stopEffect;

            this.time = new Date();

            const videoDiv = document.createElement('div');
            this._canvas = document.createElement('canvas');
            this._ctx = this._canvas.getContext('2d');

            this._desktopElement = document.createElement('video');
            this._videoElement = document.createElement('video');

            // const maxWidth = 550;
            // const newWidth = width < maxWidth ? width : maxWidth;
            const newWidth = 550;

            if (videoStream && videoStream.getVideoTracks) {
                const firstVideoTrack = videoStream.getVideoTracks()[0];
                const { height, width, frameRate } = firstVideoTrack.getSettings() ?? firstVideoTrack.getConstraints();

                this._frameRate = parseInt(frameRate || 15, 10);

                videoDiv.appendChild(this._videoElement);

                this._videoElement.width = parseInt(newWidth, 10);
                this._videoElement.height = parseInt(newWidth * (height / width), 10);
                this._videoElement.autoplay = true;
                this._videoElement.srcObject = videoStream;

                this._videoElement.play();
            }

            videoDiv.appendChild(this._desktopElement);
            if (document.body !== null) {
                document.body.appendChild(videoDiv);
            }

            // Set the video element properties

            this._frameRate = this._frameRate ?? parseInt(15, 10);

            // autoplay is not enough to start the video on Safari, it's fine to call play() on other platforms as well

            // set the style attribute of the div to make it invisible
            videoDiv.style.display = 'none';

            // Bind event handler so it is only bound once for every instance.
            this._onVideoFrameTimer = this._onVideoFrameTimer.bind(this);

            let time2 = new Date();
            console.log('>>> end of constructor: ', time2 - this.time);
            this.time = time2;
        }

        _onVideoFrameTimer(response) {
            if (response.data.id === 3) {
                this._renderVideo();
            }
        }

        _renderVideo() {
            // adjust the canvas width/height on every frame incase the window has been resized.
            const [track] = this._desktopStream.getVideoTracks();
            const { height, width } = track.getSettings() ?? track.getConstraints();

            this._canvas.width = parseInt(width, 10);
            this._canvas.height = parseInt(height, 10);

            // const newVideoWidth = this._canvas.width - this._videoElement.width;
            // const newVideoHeight = this._canvas.height * (newVideoWidth / this._canvas.width)

            this._ctx.fillStyle = '#e5e9ec';
            this._ctx.fillRect(0, 0, this._canvas.width, this._canvas.height);

            this._ctx.drawImage(this._desktopElement, 0, 0, this._canvas.width, this._canvas.height);
            this._ctx.globalAlpha = 0.6;
            this._ctx.drawImage(this._videoElement, this._canvas.width - this._videoElement.width, 0, this._videoElement.width, this._videoElement.height);

            // draw a border around the video element.
            this._ctx.beginPath();
            this._ctx.lineWidth = 2;
            this._ctx.strokeStyle = '#A9A9A9'; // dark grey
            this._ctx.rect(this._canvas.width - this._videoElement.width, 0, this._videoElement.width, this._videoElement.height);
            // this._ctx.rect(0, 0, newVideoWidth, newVideoHeight);
            this._ctx.stroke();
        }

        isEnabled(track) {
            let time3 = new Date();
            console.log('>>> check is enabled: ', time3 - this.time);
            this.time = time3;

            return track.isVideoTrack() && track.videoType === 'desktop';
        }

        startEffect(desktopStream) {
            let time4 = new Date();
            console.log('>>> start of startEffect: ', time4 - this.time);
            this.time = time4;

            const code = `
    var timer;

    onmessage = function(request) {
        switch (request.data.id) {
        case ${1}: {
            timer = setInterval(() => {
                postMessage({ id: ${3} });
            }, request.data.timeMs);
            break;
        }
        case ${2}: {
            if (timer) {
                clearInterval(timer);
            }
            break;
        }
        }
    };
`;

            const timerWorkerScript = URL.createObjectURL(new Blob([code], { type: 'application/javascript' }));

            const firstVideoTrack = desktopStream.getVideoTracks()[0];
            const { height, width } = firstVideoTrack.getSettings() ?? firstVideoTrack.getConstraints();

            // set the desktop element properties.
            this._desktopStream = desktopStream;
            this._desktopElement.width = parseInt(width, 10);
            this._desktopElement.height = parseInt(height, 10);
            this._desktopElement.autoplay = true;
            this._desktopElement.srcObject = desktopStream;

            // autoplay is not enough to start the video on Safari, it's fine to call play() on other platforms as well
            this._desktopElement.play();

            this._canvas.width = parseInt(width, 10);
            this._canvas.height = parseInt(height, 10);
            this._videoFrameTimerWorker = new Worker(timerWorkerScript, { name: 'Presenter effect worker' });
            this._videoFrameTimerWorker.onmessage = this._onVideoFrameTimer;
            this._videoFrameTimerWorker.postMessage({
                id: 1,
                timeMs: 1000 / this._frameRate
            });

            let time5 = new Date();
            console.log('>>> end of startEffect: ', time5 - this.time);
            this.time = time5;

            return this._canvas.captureStream(this._frameRate);
        }

        stopEffect() {
            console.log('>>> stopping effect');
            this._videoFrameTimerWorker.postMessage({
                id: 2
            });
            this._videoFrameTimerWorker.terminate();
        }
    }
    return PresenterEffect;
});
