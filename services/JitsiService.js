app.service('jitsiService', ['$http', '$window',
    function ($http, $window) {
        return {
            createUserElement: async function (userId) {
                try {
                    const containerDiv = document.createElement('div');
                    containerDiv.setAttribute('id', `user-container-${userId}`);

                    const videoComponent = document.createElement('video');
                    videoComponent.setAttribute('id', `video-track-${userId}`);
                    videoComponent.setAttribute('style', 'width: 200px; height: 120px');
                    videoComponent.setAttribute('autoplay', 'true');

                    const audioComponent = document.createElement('audio');
                    audioComponent.setAttribute('id', `audio-track-${userId}`);
                    audioComponent.setAttribute('autoplay', 'true');

                    containerDiv.appendChild(videoComponent);
                    containerDiv.appendChild(audioComponent);

                    const mainContainer = document.getElementById('remote-video-container');
                    mainContainer.appendChild(containerDiv);
                } catch (error) {
                    console.log('error in createUserElement:', error);
                }
            }
        };
    }])