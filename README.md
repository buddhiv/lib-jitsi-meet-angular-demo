# Lib Jitsi Meet Angular Demo

Demo application written using angularjs and lib-jitsi-meet.js

## About

This is a sample application written to demonstrate the lib-jitsi-meet features.

## How to Run

- Certificate file and the simple HTTP server script is included in the project.
- Run `python simple-server.py` in the project directory. (You can use your own method of turning up a simple HTTP server.)
- Go to `https://localhost:4443`.
