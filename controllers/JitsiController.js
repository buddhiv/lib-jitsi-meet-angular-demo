app.controller('jitsiController', ['$scope', '$window', 'jitsiService', 'PresenterEffect',
    function ($scope, $window, jitsiService, PresenterEffect) {
        $scope.title = "Jitsi Integration by Buddhi";
        $scope.tracks = {};

        let connection;
        let room;
        let localTracks;
        let screenTracks;

        console.log('changes 222');

        $scope.initJitsiConnection = async function () {
            console.log('JITSI INITIALIZING!!!');

            let config = {
                hosts: {
                    domain: 'beta.meet.jit.si',
                    muc: 'conference.beta.meet.jit.si', // FIXME: use XEP-0030
                },
                disableSimulcast: false,
                constraints: {
                    video: {
                        height: {
                            ideal: 720,
                            max: 720,
                            min: 180
                        },
                        width: {
                            ideal: 1280,
                            max: 1280,
                            min: 320
                        }
                    }
                },
                enableP2P: true,
                useStunTurn: true,
                p2p: {
                    enabled: true,
                    enableUnifiedOnChrome: true,
                    preferredCodec: 'VP9',
                    disableH264: true,
                    useStunTurn: true,
                    stunServers: [
                        { urls: 'stun:meet-jit-si-turnrelay.jitsi.net:443' }
                    ],
                },
                bosh: '//beta.meet.jit.si/http-bind',
                // The name of client node advertised in XEP-0115 'c' stanza
                clientNode: 'http://jitsi.org/jitsimeet',
                videoQuality: {
                    preferredCodec: 'VP9',
                    maxBitratesVideo: {
                        VP8: {
                            low: 200000,
                            standard: 500000,
                            high: 1500000
                        },
                        VP9: {
                            low: 100000,
                            standard: 300000,
                            high: 1200000
                        }
                    },
                },
            }

            await JitsiMeetJS.setLogLevel(JitsiMeetJS.logLevels.ERROR);

            let configCopy = config;
            configCopy.disableAudioLevels = true;
            await JitsiMeetJS.init(configCopy);

            if (JitsiMeetJS.mediaDevices.isDeviceChangeAvailable('output')) {
                JitsiMeetJS.mediaDevices.enumerateDevices(devices => {
                    console.log('media devices', devices);
                });
            }

            console.log('>>> resolution 333');

            localTracks = await JitsiMeetJS.createLocalTracks({
                devices: ['video'],
            });

            console.log('createLocalTracks video', localTracks);

            connection = new JitsiMeetJS.JitsiConnection(null, null, config);

            connection.addEventListener(
                JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED, () => {
                    console.log('connection established');

                    room = connection.initJitsiConference('tryndamere', config);

                    room.setDisplayName('tryndamere');

                    room.on(JitsiMeetJS.events.conference.CONFERENCE_JOINED, () => {
                        console.log('room conference joined');

                        room.addTrack(localTracks[0]);
                        // room.addTrack(localTracks[1]);

                        // localTracks[0].attach($(`#audio-track-local`)[0]);
                        // localTracks[1].attach($(`#video-track-local`)[0]);

                        localTracks[0].attach($(`#video-track-local`)[0]);

                        console.log('>>> localTracks[1] video track', localTracks[1]);

                        // localTracks[0].mute();
                    });
                    room.on(JitsiMeetJS.events.conference.TRACK_ADDED, (track) => {
                        console.log('room track added: ', track, track.getType());

                        // if(track.isLocal()){
                        //     localTracks = [track];
                        // }

                        track.addEventListener(JitsiMeetJS.events.track.TRACK_VIDEOTYPE_CHANGED, (videotype) => {
                            console.log(`track videotype changed to ${videotype}`)
                        });

                        track.addEventListener(JitsiMeetJS.events.track.NO_DATA_FROM_SOURCE, (data) => {
                            console.log('>>>>>>>> no data', data);
                        });

                        let participantId = 'local';
                        if (!track.isLocal()) {
                            participantId = track.getParticipantId();
                        }

                        if (!$scope.tracks[participantId]) {
                            $scope.tracks[participantId] = {};
                        }

                        if (track.getType() === 'video') {
                            $scope.tracks[participantId].videoTrack = track;
                            track.attach($(`#video-track-${participantId}`)[0]);
                        } else if (track.getType() === 'audio') {
                            $scope.tracks[participantId].audioTrack = track;
                            track.attach($(`#audio-track-${participantId}`)[0]);
                        }
                    });
                    room.on(JitsiMeetJS.events.conference.TRACK_REMOVED, (track) => {
                        console.log('room track removed: ', track);

                        const participantId = track.getParticipantId();
                        delete $scope.tracks[participantId];

                        console.log('$scope.tracks[participantId]', participantId, $scope.tracks[participantId]);
                    });
                    room.on(JitsiMeetJS.events.conference.USER_JOINED, (id, user) => {
                        console.log('room user joined: ', user, user.getId());

                        const userId = user.getId();
                        jitsiService.createUserElement(userId);
                    });
                    room.on(JitsiMeetJS.events.conference.USER_LEFT, (id) => {
                        console.log(`room user left: `, id);

                        const mainContainer = document.getElementById('remote-video-container');
                        const userContainer = document.getElementById(`user-container-${id}`);
                        mainContainer.removeChild(userContainer);
                    });
                    room.on(JitsiMeetJS.events.conference.TRACK_MUTE_CHANGED, track => {
                        console.log(`room track mute changed ${track.getType()} - ${track.isMuted()}`, track);
                    });
                    room.on(JitsiMeetJS.events.conference.DISPLAY_NAME_CHANGED, (userID, displayName) => {
                        console.log(`room display name changed ${userID} - ${displayName}`)
                    });
                    room.on(JitsiMeetJS.events.conference.TRACK_AUDIO_LEVEL_CHANGED, (userID, audioLevel) => {
                        console.log(`room track audio level changed ${userID} - ${audioLevel}`)
                    });

                    room.join();
                });

            connection.addEventListener(
                JitsiMeetJS.events.connection.CONNECTION_FAILED, (reason) => {
                    console.log('connection failed: ', reason);
                });

            connection.addEventListener(
                JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED, () => {
                    console.log('connection disconnected');
                });
            JitsiMeetJS.mediaDevices.addEventListener(
                JitsiMeetJS.events.mediaDevices.DEVICE_LIST_CHANGED, (devices) => {
                    console.log('connection device list changed');
                });

            connection.connect();
            console.log('JITSI INITIALIZED!!!');
        }

        $scope.shareScreen = async function () {
            screenTracks = await JitsiMeetJS.createLocalTracks({
                devices: ['desktop'],
            });

            // await screenTracks[0].setEffect(new PresenterEffect(localTracks[0].stream));
            await room.replaceTrack(localTracks[0], screenTracks[0]);
            // localTracks[0].dispose();
            // room.addTrack(screenTracks[0]);

            ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////

            // try {
            //     var newScreenTracks = await JitsiMeetJS.createLocalTracks({
            //         devices: ['desktop'],
            //     });

            //     await room.removeTrack(localTracks[0]);
            //     // await localTracks[0].dispose();

            //     localTracks = [];
            //     screenTracks = newScreenTracks;

            //     await room.addTrack(screenTracks[0]);
            //     screenTracks[0].attach($(`#video-track-local`)[0]);
            // } catch (error) {
            //     console.log('error in starting screenshare');
            // }
        }

        $scope.stopShareScreen = async function () {
            localTracks = await JitsiMeetJS.createLocalTracks({
                devices: ['video'],
            });

            await room.replaceTrack(screenTracks[0], localTracks[0]);
            // await screenTracks[0].dispose();
            // await room.addTrack(localTracks[0]);

            ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////

            // try {
            //     var newLocalTracks = await JitsiMeetJS.createLocalTracks({
            //         devices: ['video'],
            //     });

            //     await room.removeTrack(screenTracks[0]);
            //     // await screenTracks[0].dispose();

            //     screenTracks = [];
            //     localTracks = newLocalTracks;

            //     await room.addTrack(localTracks[0]);
            //     localTracks[0].attach($(`#video-track-local`)[0]);
            // } catch (error) {
            //     console.log('error in stopping screenshare');
            // }
        }
    }]);